import requests
import os

if __name__ == "__main__":
    for url in os.environ['url'].split(","):
        print(requests.get(url).text)
    print(requests.get(os.environ['url']).text)