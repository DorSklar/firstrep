import os
from time import sleep

if __name__ == "__main__":
    num = int(os.environ['iter_num'])
    for i in range(num):
        print (str(i)+" "+str(i*i))
        sleep(0.5)
