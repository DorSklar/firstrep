# # # main - for Jenkins exercise 

import sys 
from time import sleep

def print_powers(num):
    for i in range(num):
        print(str(i) + " " + str(i**2))
        sleep(0.5)

def main():
    
    print_powers(int(sys.argv[1]))


if __name__ == "__main__":
    main()